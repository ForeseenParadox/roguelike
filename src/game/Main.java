package game;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import game.command.Command;
import game.command.CommandConsume;
import game.command.CommandExecutor;
import game.command.CommandMove;
import game.entity.ItemEntity;
import game.entity.Player;
import game.generator.MapGenerator;
import game.item.HealthPotion;
import game.item.ItemStack;

public class Main
{

	private static MessageLog log;

	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		String lastCommand = "";

		// message buffer
		log = new MessageLog(5);

		// command executors
		List<CommandExecutor> executors = new LinkedList<CommandExecutor>();
		executors.add(new CommandMove());
		executors.add(new CommandConsume());

		// initialize map
		World m = new World(100, 100);
		MapGenerator generator = new MapGenerator();

		// use custom seed if specified
		int seedOption = findOption(args, "-s");
		if (seedOption > -1)
			generator.setRandom(new Random(args[seedOption + 1].hashCode()));

		generator.generate(m);

		m.getEntities().add(new ItemEntity(10, 15, 'A', m, new ItemStack(new HealthPotion(), 2)));
		m.getEntities().add(new ItemEntity(10, 18, 'A', m, new ItemStack(new HealthPotion(), 2)));

		Player p = new Player(5, 5, m);
		m.getEntities().add(p);

		do
		{

			// for (int i = 0; i < 50; i++)
			// System.out.println();

			String[] mapOutput = m.output('#', p.getX(), p.getY(), 40, 20);
			String[] inventoryOutput = p.getInventory().output('#', 4);
			String[] statsOutput = p.getStats().output('#');

			// add inventory to map output lines
			// the String.format is a fancy way of adding three spaces
			for (int i = 2 + statsOutput.length + 1, j = 0; j < inventoryOutput.length && i < mapOutput.length; i++, j++)
				mapOutput[i] += String.format("%3s", "") + inventoryOutput[j];

			// add stats to map output lines
			// the String.format is a fancy way of adding three spaces
			for (int i = 2, j = 0; j < statsOutput.length && i < mapOutput.length; i++, j++)
				mapOutput[i] += String.format("%3s", "") + statsOutput[j];

			// print out the map output plus the inventory output plus the stats
			for (String x : mapOutput)
				System.out.println(x);

			System.out.print(ConsoleColor.ANSI_YELLOW + "Command: ");
			lastCommand = in.nextLine();

			try
			{
				new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			} catch (IOException e)
			{
				e.printStackTrace();
			}

			// handle commands
			for (CommandExecutor ex : executors)
				ex.execute(new Command(lastCommand), p, log);

			// print out the message log
			for (String x : log.getOutput())
				if (x != null)
					System.out.println(x);

			p.getInventory().updateInventory();
		} while (!lastCommand.equalsIgnoreCase("quit"));

		in.close();
	}

	public static MessageLog getLog()
	{
		return log;
	}

	public static int findOption(String[] args, String option)
	{
		for (int i = 0; i < args.length; i++)
			if (args[i].equals(option))
				return i;
		return -1;
	}

}
