package game.generator;

public class Room
{

	public int x, y, width, height;
	public Room left, right;

	public Room()
	{
	}

	public Room(int x, int y, int width, int height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public boolean isLeaf()
	{
		return left == null && right == null;
	}

	@Override
	public String toString()
	{
		return "Room [x=" + x + ", y=" + y + ", width=" + width + ", height=" + height + ", left=" + left + ", right=" + right + "]";
	}

}