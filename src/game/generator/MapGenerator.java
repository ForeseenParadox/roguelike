package game.generator;

import java.util.Random;

import game.Cell;
import game.ConsoleColor;
import game.World;
import game.entity.ItemEntity;
import game.item.HealthPotion;
import game.item.Item;
import game.item.ItemStack;

public class MapGenerator
{

	public static final int MIN_ROOM_SIZE = 10;
	public static final int MAX_ROOM_SIZE = 30;

	private Random random;

	public MapGenerator()
	{
		this(new Random());
	}

	public MapGenerator(Random random)
	{
		this.random = random;
	}

	public Random getRandom()
	{
		return random;
	}

	public void setRandom(Random random)
	{
		this.random = random;
	}

	public void generate(World map)
	{
		for (int i = 0; i < map.getHeight(); i++)
		{
			for (int j = 0; j < map.getWidth(); j++)
			{
				map.getData()[i][j] = new Cell('.', false, ConsoleColor.ANSI_CYAN);
			}
		}

		Room parent = new Room(0, 0, map.getWidth(), map.getHeight());
		divide(parent);
		traverseAndFillRooms(map, parent, parent);
		traverseAndFillHalls(map, parent, parent);
		populate(map, parent);
	}

	public void populate(World map, Room parent)
	{
		int items = 15;
		while (items > 0)
		{
			Room randomLeaf = randomLeaf(parent);
			int randX = random.nextInt(randomLeaf.width - 2) + randomLeaf.x + 1, randY = random.nextInt(randomLeaf.height - 2) + randomLeaf.y + 1;
			int randItemId = random.nextInt(1);
			Item it = null;
			if (randItemId == 0)
				it = new HealthPotion();
			map.getEntities().add(new ItemEntity(randX, randY, 'A', map, new ItemStack(it, 1)));
			items--;
		}
	}

	public Room randomLeaf(Room parent)
	{
		Room room = parent;
		while (!room.isLeaf())
		{
			if (random.nextBoolean())
				room = room.left;
			else
				room = room.right;
		}
		return room;
	}

	public Room getSibling(Room root, Room node)
	{
		if (root == null)
			return null;
		else if (root.left == node)
			return root.right;
		else if (root.right == node)
			return root.left;
		else
		{
			Room left = getSibling(root.left, node);
			Room right = getSibling(root.right, node);
			if (left != null)
				return left;
			else if (right != null)
				return right;
		}
		return null;
	}

	public void traverseAndFillRooms(World map, Room root, Room n)
	{
		if (n.left == null && n.right == null)
		{
			// fill in walls
			for (int i = n.y; i < n.y + n.height; i++)
			{
				for (int j = n.x; j < n.x + n.width; j++)
				{
					if (i == n.y || i == n.y + n.height - 1 || j == n.x || j == n.x + n.width - 1)
					{
						map.getData()[i][j] = new Cell('%', true, ConsoleColor.ANSI_CYAN);
					}
				}
			}
		} else
		{
			traverseAndFillRooms(map, root, n.left);
			traverseAndFillRooms(map, root, n.right);
		}
	}

	public void traverseAndFillHalls(World map, Room root, Room n)
	{

		if (n.left == null && n.right == null)
		{

			// fill in hallways
			Room sibling = getSibling(root, n);
			if (sibling.y > n.y)
			{
				// divided horizontally
				int randX = (int) (random.nextDouble() * ((n.width - 2) - n.x)) + n.x + 1;
				int midY = n.y + n.height / 2, midYS = sibling.y + sibling.height / 2;
				for (int j = midY; j <= midYS; j++)
				{
					map.getData()[j][randX] = new Cell('.', false, ConsoleColor.ANSI_CYAN);
				}

			} else if (sibling.y < n.y)
			{
				// divided horizontally
				int randX = (int) (random.nextDouble() * ((n.width - 2) - n.x)) + n.x + 1;
				int midY = n.y + n.height / 2, midYS = sibling.y + sibling.height / 2;
				for (int j = midYS; j <= midY; j++)
				{
					map.getData()[j][randX] = new Cell('.', false, ConsoleColor.ANSI_CYAN);
				}
			}
			if (sibling.x > n.x)
			{
				// divided horizontally
				int randY = (int) (random.nextDouble() * ((n.height - 2) - n.y)) + n.y + 1;
				int midX = n.x + n.width / 2, midXS = sibling.x + sibling.width / 2;
				for (int j = midX; j <= midXS; j++)
				{
					map.getData()[randY][j] = new Cell('.', false, ConsoleColor.ANSI_CYAN);
				}

			} else if (sibling.x < n.x)
			{
				// divided horizontally
				int randY = (int) (random.nextDouble() * ((n.height - 2) - n.y)) + n.y + 1;
				int midX = n.x + n.width / 2, midXS = sibling.x + sibling.width / 2;
				for (int j = midXS; j <= midX; j++)
				{
					map.getData()[randY][j] = new Cell('.', false, ConsoleColor.ANSI_CYAN);
				}

			}
		} else
		{
			traverseAndFillHalls(map, root, n.left);
			traverseAndFillHalls(map, root, n.right);
		}
	}

	public void divide(Room parent)
	{
		if (parent.width > MAX_ROOM_SIZE && parent.height > MAX_ROOM_SIZE)
		{
			if (random.nextDouble() > 0.5)
				divideHorizontal(parent);
			else
				divideVertical(parent);
		} else if (parent.width > MAX_ROOM_SIZE && parent.height <= MAX_ROOM_SIZE)
		{
			divideHorizontal(parent);
		} else if (parent.width <= MAX_ROOM_SIZE && parent.height > MAX_ROOM_SIZE)
		{
			divideVertical(parent);
		}
	}

	public void divideVertical(Room parent)
	{
		int minY = parent.y + MIN_ROOM_SIZE;
		int maxY = parent.y + parent.height - MIN_ROOM_SIZE;
		int randomCut = (int) (random.nextDouble() * (maxY - minY + 1)) + minY;
		Room bottom = new Room(parent.x, parent.y, parent.width, randomCut - parent.y);
		Room top = new Room(parent.x, randomCut, parent.width, parent.y + parent.height - randomCut);
		parent.left = bottom;
		parent.right = top;
		divide(bottom);
		divide(top);
	}

	public void divideHorizontal(Room parent)
	{
		int minX = parent.x + MIN_ROOM_SIZE;
		int maxX = parent.x + parent.width - MIN_ROOM_SIZE;
		int randomCut = (int) (random.nextDouble() * (maxX - minX + 1)) + minX;
		Room left = new Room(parent.x, parent.y, randomCut - parent.x, parent.height);
		Room right = new Room(randomCut, parent.y, parent.x + parent.width - randomCut, parent.height);
		parent.left = left;
		parent.right = right;
		divide(left);
		divide(right);
	}

}