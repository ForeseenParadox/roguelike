package game.entity;

import game.World;
import game.item.ItemStack;

public class ItemEntity extends Entity
{

	private ItemStack items;

	public ItemEntity(int x, int y, char character, World world, ItemStack items)
	{
		super(x, y, character, world);
		this.items = items;
	}

	public ItemStack getItems()
	{
		return items;
	}

	public void setItems(ItemStack items)
	{
		this.items = items;
	}

}
