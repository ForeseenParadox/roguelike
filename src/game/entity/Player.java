package game.entity;

import game.ConsoleColor;
import game.Main;
import game.World;
import game.item.Inventory;
import game.item.ItemStack;

public class Player extends MobileEntity
{

	public static final int INVENTORY_SIZE = 12;
	public static final int INVENTORY_ROWS = 4;

	private Inventory inventory;

	public Player(int x, int y, World world)
	{
		super(x, y, '@', world);
		getStats().setHp(20000);
		getStats().setStrength(0);
		getStats().setDefense(0);
		getStats().setGold(200);

		inventory = new Inventory(INVENTORY_SIZE);

	}

	public Inventory getInventory()
	{
		return inventory;
	}

	public void setInventory(Inventory inventory)
	{
		this.inventory = inventory;
	}

	@Override
	public void setPosition(int x, int y)
	{
		Entity e = getWorld().getEntityAtLocation(x, y);
		if (e instanceof ItemEntity)
		{
			ItemStack items = ((ItemEntity) e).getItems();
			inventory.addItem(items);
			getWorld().getEntities().remove(e);

			Main.getLog().addMessage(ConsoleColor.ANSI_CYAN + "You picked up " + items.getCount() + " " + items.getItem() + "(s)!");
		}

		super.setPosition(x, y);
	}

}