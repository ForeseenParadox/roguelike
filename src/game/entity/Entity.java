package game.entity;

import game.World;

public class Entity
{

	private int x;
	private int y;
	private char character;
	private World world;

	public Entity(int x, int y, char character, World world)
	{
		this.x = x;
		this.y = y;
		this.character = character;
		this.world = world;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public char getCharacter()
	{
		return character;
	}

	public World getWorld()
	{
		return world;
	}

	public void setX(int x)
	{
		setPosition(x, this.y);
	}

	public void setY(int y)
	{
		setPosition(this.x, y);
	}

	public void setPosition(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public void setCharacter(char character)
	{
		this.character = character;
	}

	public void setWorld(World world)
	{
		this.world = world;
	}

	public boolean canMove(int dx, int dy)
	{
		return !world.isCollidable(getX() + dx, getY() + dy);
	}

}