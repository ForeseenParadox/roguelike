package game.entity;

import game.ConsoleColor;

public class Stats
{

	private int level;
	private int exp;
	private int hp;
	private int gold;
	private int strength;
	private int defense;

	public Stats()
	{
		setExp(1);
	}

	public int getLevel()
	{
		return level;
	}

	public int getExp()
	{
		return exp;
	}

	public int getHp()
	{
		return hp;
	}

	public int getGold()
	{
		return gold;
	}

	public int getStrength()
	{
		return strength;
	}

	public int getDefense()
	{
		return defense;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public void setExp(int exp)
	{
		this.exp = exp;
		if (exp >= getExpNeeded(level + 1))
			level++;
	}

	public void changeExp(int amount)
	{
		setExp(exp + amount);
	}

	public void setHp(int hp)
	{
		this.hp = hp;
	}

	public void setGold(int gold)
	{
		this.gold = gold;
	}

	public void setStrength(int strength)
	{
		this.strength = strength;
	}

	public void setDefense(int defense)
	{
		this.defense = defense;
	}

	public static int getExpNeeded(int level)
	{
		return (5 * level * level * level) / 4;
	}

	public String[] output(char decor)
	{
		StringBuilder[] builder = new StringBuilder[5];
		for (int i = 0; i < builder.length; i++)
			builder[i] = new StringBuilder();

		final int SLOT_LENGTH = 9 + 1 + 11 + 1 + 8 + 1 + 11;
		final int DECOR_LENGTH = 2;
		final int LINE_LENGTH = SLOT_LENGTH + DECOR_LENGTH;

		if (decor > 0)
		{
			builder[0].append(ConsoleColor.ANSI_CYAN + String.format("%-" + LINE_LENGTH + "s", "Stats").replace(" ", "" + decor));
			builder[1].append(ConsoleColor.ANSI_CYAN + decor);
			builder[2].append(ConsoleColor.ANSI_CYAN + decor);
			builder[3].append(ConsoleColor.ANSI_CYAN + decor);
			builder[4].append(ConsoleColor.ANSI_CYAN + String.format("%-" + LINE_LENGTH + "s", "").replace(" ", "" + decor));
		}
		builder[1].append(String.format(ConsoleColor.ANSI_GREEN + "%-9s " + ConsoleColor.ANSI_CYAN + "%,11d " + ConsoleColor.ANSI_GREEN + "%-8s " + ConsoleColor.ANSI_CYAN + "%,11d", "Level:", getLevel(), "Exp:", getExp()));
		if (decor > 0)
			builder[1].append(decor);
		builder[2].append(String.format(ConsoleColor.ANSI_GREEN + "%-9s " + ConsoleColor.ANSI_CYAN + "%,11d " + ConsoleColor.ANSI_GREEN + "%-8s " + ConsoleColor.ANSI_CYAN + "%,11d", "HP:", getHp(), "Gold:", getGold()));
		if (decor > 0)
			builder[2].append(decor);
		builder[3].append(String.format(ConsoleColor.ANSI_GREEN + "%-9s " + ConsoleColor.ANSI_CYAN + "%,11d " + ConsoleColor.ANSI_GREEN + "%-8s " + ConsoleColor.ANSI_CYAN + "%,11d", "Strength:", getStrength(), "Defense:", getDefense()));
		if (decor > 0)
			builder[3].append(decor);
		
		String[] result = new String[builder.length];
		for (int i = 0; i < builder.length; i++)
			result[i] = builder[i].toString();
		return result;
	}

}
