package game.entity;

import game.World;

public class MobileEntity extends Entity
{

	private Stats stats;

	public MobileEntity(int x, int y, char character, World world)
	{
		super(x, y, character, world);

		stats = new Stats();
	}

	public Stats getStats()
	{
		return stats;
	}

	public void setStats(Stats stats)
	{
		this.stats = stats;
	}

}
