package game;


public class Cell
{

	private char character;
	private boolean collidable;
	private String color;

	public Cell(char character, boolean collidable, String color)
	{
		this.character = character;
		this.collidable = collidable;
		this.color = color;
	}

	public char getCharacter()
	{
		return character;
	}

	public boolean isCollidable()
	{
		return collidable;
	}

	public String getColor()
	{
		return color;
	}

	public void setCharacter(char character)
	{
		this.character = character;
	}

	public void setCollidable(boolean collidable)
	{
		this.collidable = collidable;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

}
