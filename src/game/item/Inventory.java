package game.item;

import game.ConsoleColor;

public class Inventory
{

	private int size;
	private ItemStack[] items;

	public Inventory(int size)
	{
		this.size = size;
		items = new ItemStack[size];
	}

	public int getSize()
	{
		return size;
	}

	public ItemStack[] getItems()
	{
		return items;
	}

	public ItemStack getItemStackByItem(Item item)
	{
		for (ItemStack it : items)
			if (it != null && it.getItem().equals(item))
				return it;
		return null;
	}

	public int findVacantSlot()
	{
		for (int i = 0; i < items.length; i++)
			if (items[i] == null)
				return i;
		return -1;
	}

	public void updateInventory()
	{
		for (int i = 0; i < size; i++)
		{
			if (items[i] != null && items[i].getCount() <= 0)
				items[i] = null;
		}
	}

	public boolean addItem(ItemStack itemStack)
	{
		// search
		if (itemStack != null)
		{
			ItemStack currentStack = getItemStackByItem(itemStack.getItem());
			if (currentStack == null)
			{
				int vacantSlot = findVacantSlot();
				if (vacantSlot > -1)
				{
					items[vacantSlot] = itemStack;
					return true;
				} else
				{
					return false;
				}
			} else
			{
				currentStack.setCount(itemStack.getCount() + currentStack.getCount());
			}
		} else
		{
			throw new NullPointerException("ItemStack can not be null");
		}
		return false;
	}

	public String[] output(char decor, int rows)
	{
		StringBuilder[] builder = new StringBuilder[rows + 2];
		for (int i = 0; i < builder.length; i++)
			builder[i] = new StringBuilder();
		final int COLS = size / rows;

		final int SLOT_LENGTH = 5 + 1 + 15 + 1 + 5;
		final int DECOR_LENGTH = 2;
		final int LINE_LENGTH = COLS * SLOT_LENGTH + DECOR_LENGTH;

		for (int i = 0; i < rows; i++)
		{
			if (decor > 0)
				builder[i + 1].append(ConsoleColor.ANSI_CYAN + decor + ConsoleColor.ANSI_RESET);

			for (int j = 0; j < COLS; j++)
			{
				String indexString = "[" + (i * COLS + j + 1) + "]";
				if (items[i * COLS + j] == null)
					builder[i + 1].append(String.format(ConsoleColor.ANSI_GREEN + "%-5s " + ConsoleColor.ANSI_CYAN + "%-21s", indexString, "Empty"));
				else
					builder[i + 1].append(String.format(ConsoleColor.ANSI_GREEN + "%-5s " + ConsoleColor.ANSI_CYAN + "%-15s %-5d", indexString, items[i].getItem().toString(), items[i].getCount()));
			}
			if (decor > 0)
				builder[i + 1].append(ConsoleColor.ANSI_CYAN + decor + ConsoleColor.ANSI_RESET);
		}

		String[] result = new String[builder.length];
		for (int i = 0; i < builder.length; i++)
			result[i] = builder[i].toString();

		String topString = ConsoleColor.ANSI_CYAN + String.format("%-" + LINE_LENGTH + "s", "Inventory");
		if (decor > 0)
			topString = topString.replace(" ", "" + decor);
		result[0] = topString;

		builder[0].append(ConsoleColor.ANSI_CYAN + topString + ConsoleColor.ANSI_RESET);

		if (decor > 0)
			result[result.length - 1] = ConsoleColor.ANSI_CYAN + String.format("%-" + LINE_LENGTH + "s", "").replaceAll(".", "" + decor) + ConsoleColor.ANSI_RESET;

		return result;
	}

}
