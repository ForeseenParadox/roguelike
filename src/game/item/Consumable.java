package game.item;

import game.entity.MobileEntity;

public abstract class Consumable extends Item
{

	public Consumable(String name)
	{
		super(name);
	}

	public abstract void consume(MobileEntity mob);

}
