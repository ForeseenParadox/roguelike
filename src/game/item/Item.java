package game.item;

public abstract class Item
{

	private String name;
	private int value;

	public Item(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public int getValue()
	{
		return value;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setValue(int value)
	{
		this.value = value;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Item)
		{
			Item it = (Item) obj;
			return it.getName().equals(name);
		} else
		{
			return false;
		}
	}

	@Override
	public String toString()
	{
		return name;
	}

}
