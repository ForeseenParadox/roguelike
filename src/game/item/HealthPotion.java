package game.item;

import game.entity.MobileEntity;

public class HealthPotion extends Consumable
{

	public HealthPotion()
	{
		super("Health Potion");
	}

	@Override
	public void consume(MobileEntity mob)
	{
		mob.getStats().setHp(mob.getStats().getHp() + 2);
	}

}
