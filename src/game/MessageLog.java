package game;

public class MessageLog
{

	private String[] messageBuffer;

	public MessageLog(int bufferSize)
	{
		messageBuffer = new String[bufferSize];
	}

	public String[] getMessageBuffer()
	{
		return messageBuffer;
	}

	public void addMessage(String message)
	{
		for (int i = 0; i < messageBuffer.length - 1; i++)
			messageBuffer[i] = messageBuffer[i + 1];
		messageBuffer[messageBuffer.length - 1] = message;
	}

	public String[] getOutput()
	{
		String[] result = new String[messageBuffer.length];
		for (int i = 0; i < result.length; i++)
			result[i] = messageBuffer[i];
		return result;
	}

}