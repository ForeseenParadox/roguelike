package game.command;

public class Command
{

	private String command;
	private String[] args;
	public int argCount;

	public Command(String rawCommand)
	{
		String[] unparsedTokens = rawCommand.split(" ");
		command = unparsedTokens[0];
		if (unparsedTokens.length > 1)
		{
			args = new String[unparsedTokens.length - 1];
			for (int i = 1; i < unparsedTokens.length; i++)
				args[i - 1] = unparsedTokens[i];
			argCount = args.length;
		} else
		{
			argCount = 0;
		}
	}

	public String getCommand()
	{
		return command;
	}

	public String[] getArgs()
	{
		return args;
	}

	public int getArgCount()
	{
		return argCount;
	}

	public void setCommand(String command)
	{
		this.command = command;
	}

	public void setArgs(String[] args)
	{
		this.args = args;
		argCount = args.length;
	}

}