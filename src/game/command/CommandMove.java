package game.command;

import game.ConsoleColor;
import game.MessageLog;
import game.entity.Entity;
import game.entity.Player;

public class CommandMove implements CommandExecutor
{

	public CommandMove()
	{
	}

	public void execute(Command command, Entity executing, MessageLog log)
	{
		if (command.getCommand().equalsIgnoreCase("w") || command.getCommand().equalsIgnoreCase("s") || command.getCommand().equalsIgnoreCase("a") || command.getCommand().equalsIgnoreCase("d"))
		{
			if (command.getArgCount() != 0)
			{
				log.addMessage(ConsoleColor.ANSI_CYAN + "Incorrect args! Just specify either w, s, a, or d to move.");
			} else
			{
				if (executing instanceof Player)
				{
					Player p = (Player) executing;
					if (command.getCommand().equals("w"))
					{
						p.getStats().changeExp(1);
						p.setY(p.getY() - 1);
					} else if (command.getCommand().equals("s"))
					{
						p.getStats().changeExp(1);
						p.setY(p.getY() + 1);
					} else if (command.getCommand().equals("a"))
					{
						p.getStats().changeExp(1);
						p.setX(p.getX() - 1);
					} else if (command.getCommand().equals("d"))
					{
						p.getStats().changeExp(1);
						p.setX(p.getX() + 1);
					}
				}
			}

		}
	}
}
