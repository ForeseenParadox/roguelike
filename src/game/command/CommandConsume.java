package game.command;

import game.MessageLog;
import game.entity.Entity;
import game.entity.Player;
import game.item.Consumable;
import game.item.ItemStack;

public class CommandConsume implements CommandExecutor
{

	public CommandConsume()
	{

	}

	@Override
	public void execute(Command command, Entity executing, MessageLog log)
	{
		if (command.getCommand().equalsIgnoreCase("c"))
		{

			if (command.getArgCount() == 1)
			{
				try
				{

					int slot = Integer.parseInt(command.getArgs()[0]);
					Player p = (Player) executing;
					if (slot >= 1 && slot <= p.getInventory().getSize())
					{
						// p.getInventory().getItems()[slot].
						ItemStack itemStack = p.getInventory().getItems()[slot - 1];
						if (itemStack != null && itemStack.getItem() instanceof Consumable)
						{
							Consumable c = (Consumable) itemStack.getItem();
							c.consume(p);
							itemStack.setCount(itemStack.getCount() - 1);

							log.addMessage("You have consumed 1 " + itemStack.getItem());
						} else
						{
							if (itemStack == null)
								log.addMessage("There is no item located at slot " + slot);
							else
								log.addMessage("The item " + itemStack.getItem().toString() + " is not a consumable!");
						}
					} else
					{
						log.addMessage("Invalid slot specified! Specifiy a slot from 1 to " + p.getInventory().getSize() + ".");
					}
				} catch (Exception e)
				{
					log.addMessage("Failed to interpret " + command.getArgs()[1] + " as an integer value.");
				}
			} else
			{
				log.addMessage("Invalid args! Command: c <slot>");
			}
		}
	}

}
