package game.command;

import game.MessageLog;
import game.entity.Entity;

public interface CommandExecutor
{
	
	public void execute(Command command, Entity executing, MessageLog log);

}