package game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class LineCounter.
 */
public class LineCounter
{

	/** The Constant PATH. */
	public static final String PATH = "src";

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{

		System.out.println(PATH);

		List<File> allFiles = listAllFiles(new File(PATH));
		int lines = 0;
		int characters = 0;

		// bubble sort the files in terms of lexicographic order
		for (int i = allFiles.size() - 1; i >= 0; i--)
		{
			for (int j = 0; j < i; j++)
			{
				if (allFiles.get(j).getName().compareTo(allFiles.get(j + 1).getName()) > 0)
				{
					File temp = allFiles.get(j);
					allFiles.set(j, allFiles.get(j + 1));
					allFiles.set(j + 1, temp);
				}
			}
		}

		for (File f : allFiles)
		{
			int i = lineCount(f);
			lines += i;
		}

		for (File f : allFiles)
		{
			characters += charCount(f);
		}

		System.out.println(lines + " lines of code in " + allFiles.size() + " files, avg = " + ((double) lines / allFiles.size()));
		System.out.println("Char count: " + characters + ", avg = " + (characters / allFiles.size()));

	}

	/**
	 * Char count.
	 *
	 * @param file the file
	 * @return the int
	 */
	public static int charCount(File file)
	{
		try
		{
			FileInputStream input = null;
			input = new FileInputStream(file);

			int i = 0;

			while (input.read() != -1)
			{
				i++;
			}

			input.close();

			return i;
		} catch (IOException e)
		{
			e.printStackTrace();
			return -1;
		}

	}

	/**
	 * Line count.
	 *
	 * @param file the file
	 * @return the int
	 */
	public static int lineCount(File file)
	{
		BufferedReader stream = null;
		try
		{
			stream = new BufferedReader(new FileReader(file));
			int count = 0;
			String line = "";
			while ((line = stream.readLine()) != null)
			{
				if (line.contains("System.out"))
				{
					System.out.println("Sprite batch at line in " + file.getName() + " at line " + count);
					System.out.println(line);
					System.out.println();
				}
				if (!line.equals(""))
				{
					count++;
				} else
				{
				}
			}
			stream.close();
			return count;
		} catch (Exception e)
		{
			return 0;
		}
	}

	/**
	 * List all files.
	 *
	 * @param parent the parent
	 * @return the list
	 */
	public static List<File> listAllFiles(File parent)
	{
		if (parent != null)
		{
			List<File> res = new ArrayList<File>();
			for (File f : parent.listFiles())
			{
				String n = f.getName();
				if (f.isDirectory())
				{
					res.addAll(listAllFiles(f));
				} else if (n.contains(".java") || n.contains(".vs") || n.contains(".fs") || n.contains(".h") || n.contains(".cpp"))
				{
					System.out.println(n);
					res.add(f);
				}

			}
			return res;
		}
		return null;
	}
}
