package game;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import game.entity.Entity;

public class World
{

	private int width;
	private int height;
	private Cell[][] data;
	private List<Entity> entities;

	public World(int width, int height)
	{
		this.width = width;
		this.height = height;
		this.data = new Cell[height][width];
		entities = new ArrayList<Entity>();
	}

	public World(String path)
	{
		try
		{
			Scanner reader = new Scanner(new File(path));
			width = reader.nextInt();
			height = reader.nextInt();
			data = new Cell[height][width];
			Set<Character> collidables = new HashSet<Character>();
			int collidableCount = reader.nextInt();
			reader.nextLine();
			for (int i = 0; i < collidableCount; i++)
				collidables.add(reader.nextLine().charAt(0));

			for (int i = 0; i < height; i++)
			{
				String line = String.format("%-" + width + "s", reader.nextLine());
				for (int j = 0; j < width; j++)
				{
					char c = line.charAt(j);
					data[i][j] = new Cell(c, collidables.contains(c), ConsoleColor.ANSI_CYAN);
				}
			}

			reader.close();

		} catch (FileNotFoundException e)
		{
			System.err.println("Unable to read file at path " + path);
			e.printStackTrace();
		}

		// load entities
		entities = new ArrayList<Entity>();
	}

	public int gcd(int one, int two)
	{
		if (two == 0)
			return one;
		else
			return gcd(two, one % two);
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public Cell[][] getData()
	{
		return data;
	}

	public boolean isCollidable(int x, int y)
	{
		return data[y][x].isCollidable();
	}

	public List<Entity> getEntities()
	{
		return entities;
	}

	public Entity getEntityAtLocation(int x, int y)
	{
		for (Entity e : entities)
			if (e.getX() == x && e.getY() == y)
				return e;
		return null;
	}

	public boolean pointInBounds(int x, int y)
	{
		return x >= 0 && x < width && y >= 0 && y < height;
	}

	public String getDataAtPoint(int x, int y)
	{
		Entity e = getEntityAtLocation(x, y);
		if (e != null)
			return "" + e.getCharacter();
		else
		{
			if (data[y][x].getColor() != null)
				return data[y][x].getColor() + data[y][x].getCharacter();
			else
				return "" + data[y][x].getCharacter();
		}
	}

	public String[] output(char decor, int viewX, int viewY, int viewWidth, int viewHeight)
	{
		StringBuilder[] output = new StringBuilder[viewHeight + 2];
		int leftX = viewX - viewWidth / 2;
		int downY = viewY - viewHeight / 2;
		int rightX = viewX + viewWidth / 2;
		int upY = viewY + viewHeight / 2;

		for (int i = 0; i < output.length; i++)
			output[i] = new StringBuilder();

		// add top decor
		if (decor > 0)
			for (int j = 0; j < viewWidth + 2; j++)
				output[0].append(ConsoleColor.ANSI_CYAN + decor + ConsoleColor.ANSI_RESET);

		for (int i = downY; i < upY; i++)
		{
			int ii = i - downY + 1;

			if (decor > 0)
				output[ii].append(ConsoleColor.ANSI_CYAN + decor + ConsoleColor.ANSI_RESET);

			for (int j = leftX; j < rightX; j++)
			{
				// int jj = j - leftX;
				if (pointInBounds(j, i))
				{
					output[ii].append(getDataAtPoint(j, i));
				} else
				{
					output[ii].append(" ");
				}

			}

			if (decor > 0)
				output[ii].append(ConsoleColor.ANSI_CYAN + decor + ConsoleColor.ANSI_RESET);
		}

		// add bottom decor
		if (decor > 0)
			for (int j = 0; j < viewWidth + 2; j++)
				output[output.length - 1].append(ConsoleColor.ANSI_CYAN + decor + ConsoleColor.ANSI_RESET);

		String[] result = new String[output.length];
		for (int i = 0; i < output.length; i++)
			result[i] = output[i].toString();

		return result;
	}

}
